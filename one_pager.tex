%%% Grant formatting macros
\documentclass[11pt]{report}
\usepackage[utf8]{inputenc}
\usepackage[usenames,dvipsnames]{xcolor}

\usepackage[pdftex]{graphicx}
\usepackage[pdftex,pdfpagemode=None,colorlinks,linkcolor=RoyalBlue,citecolor=RoyalBlue,urlcolor=RoyalBlue]{hyperref}
\usepackage[margin=2cm,a4paper]{geometry}
\setlength{\fboxrule}{1.25pt}

\newcommand{\sep}{$\bullet$ }

\usepackage[compact]{titlesec}
\usepackage{paralist}

\renewcommand*{\thefootnote}{\fnsymbol{footnote}}

\usepackage{fancyhdr}
\usepackage{times}
\usepackage{color}

\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}

\fancyhead[L]{Jan Zeman's CV}
\fancyhead[R]{Page~\thepage/1}

\newcommand{\myResearcher}[2]{%
\bigskip\noindent%
{\large \bfseries #1}~(*#2)%
}

\newcommand{\myIDs}[1]{%
\smallskip\noindent #1}

\newcommand{\WWW}[2]{Website: \href{http://#1}{#2}}
\newcommand{\RID}[1]{ResearcherID:
\href{http://www.researcherid.com/rid/#1}{#1}}

\newcommand{\myEducation}[1]{%
\smallskip\noindent%
{\bf Education:}~#1}

\newcommand{\myResearchInterests}[1]{%
\smallskip\noindent%
{\bf Research interests:} #1}

\newcommand{\myPosition}[1]{%
\smallskip\noindent%
{\bf Position:} #1}

\newcommand{\myAwards}[1]{%
\smallskip\noindent{\bf Awards:} #1}

\newcommand{\myStudents}[1]{%
\smallskip\noindent{\bf Selected past students:} #1}

\newcommand{\myTalks}[1]{%
\smallskip\noindent%
{\bf Selected invited presentations:} #1}

\newcommand{\myService}[1]{%
\smallskip\noindent%
{\bf Service to the community:} #1}

\newcommand{\myStaysAbroad}[1]{%
\smallskip\noindent%
{\bf Stays abroad:} #1}

\newcommand{\myPublicationRecord}[3]{%
\smallskip\noindent%
{\bf Publication record:} 
#1; #2 citations, $h$-index #3}

\newcommand{\myConsulting}[1]{%
\smallskip\noindent%
{\bf Consulting and contracted research:} #1}

\newcommand{\myFundingID}[1]{%
\smallskip\noindent%
{\bf Funding record:} #1}

\newcommand{\gacrHead}{%
\vspace*{-7mm}
%
\noindent
{\bf \Large Curriculum Vitae}%
\footnote{The number or journal articles, citations~(including
self-citations), and $h$-index are determined according to the Clarivate Web of
Knowledge\textregistered~database, as of 17 December 2024.}}

\begin{document}

\gacrHead

\myResearcher{prof. Ing. Jan Zeman, Ph.D.}{1976}

\myIDs{\WWW{http://mech.fsv.cvut.cz/~zemanj/}{mech.fsv.cvut.cz/~zemanj},
\RID{A-1452-2009}}

\smallskip

\myPosition{2019 \sep Professor at the Czech
Technical University in Prague, Faculty of Civil Engineering, Department of
Mechanics}

\smallskip

\myEducation{2003 \sep Ph.D. in Theory of Structures, Klokner Institute, Czech
Technical University in Prague; 2009 \sep Habilitation in Theory of Building
Materials and Structures, Faculty of Civil Engineering, Czech
Technical University in Prague; 
2018 \sep Professor of Theory of Building
Materials and Structures, Faculty of Civil Engineering, Czech
Technical University in Prague
}

\smallskip

\myResearchInterests{random heterogeneous materials \sep mathematical modeling
of engineering problems~\sep stochastic and deterministic optimization }

\smallskip

\myAwards{2003~\sep~Ivo Babu\v{s}ka Prize for the best Ph.D.
thesis in applied mathematics and computational mechanics, Czech Association for
Mechanics and Union of Czech Mathematicians and Physicists; 
2008~\sep 2007~Highlight Paper Award, Modelling and Simulation in Materials Science and Engineering, Institute of Physics Publishing; 2008~\sep Rector
Prize for Applied Research for computational assessment of Charles Bridge in
Prague~(member of the team led by Professor Jiří Šejnoha), Czech Technical
University in Prague}

\smallskip

\myStudents{%
2008 \sep Zahra Sharif-Khodaei, Ph.D. thesis on {\em Modelling of
functionally graded materials} (jointly with Petr P. Procházka),
Czech Technical University in Prague, currently Full Professor at Imperial
College London; 2013 \sep Jaroslav Vondřejc, Ph.D. thesis on {\em FFT-based
method for homogenization of periodic media: Theory and applications}~(jointly with Ivo Marek), Czech Technical University in Prague, currently Data Scientist at IAV GmbH; 
2019 \sep Martin Doškář, Ph.D. thesis on \emph{Wang tiling for modelling of heterogeneous materials}~(jointly with Jan Novák), currently Post-doctoral researcher at CTU in Prague; 
2021 \sep Marek Tyburec, Ph.D. thesis on \emph{Modular-topology optimization of structures and mechanisms}~(jointly with Matěj Lepš), currently Post-doctoral researcher at CTU in Prague and Czech Academy of Sciences;
2022 \sep Martin Ladecký, Ph.D. thesis on \emph{Advanced spectral methods for computational homogenisation of periodic media}~(jointly with Ivana Pultarová),
currently Post-doctoral researcher at University of Freiburg} 

\smallskip

\myStaysAbroad{2003 \sep Visiting Scholar, Swiss Federal Institute of Technology
at Lausanne (EPFL), 2005--2006 \sep Marie-Curie Fellow, Eindhoven University of
Technology, Group of Mechanics of Materials at Department of Mechanical
Engineering; 2007 \sep Visiting Professor, Ecole normale sup\'{e}rieure de
Cachan, Laboratory of Mechanics and Technology at Department of Civil
Engineering; 2015~\sep Visiting Researcher, University of Luxembourg, Research
Unit in Engineering Sciences}

\smallskip

\myTalks{%
    2016 \sep AIME@CZ - Czech Workshop on Applied Mathematics in Engineering, Prague~(invited lecture); 2019 \sep COMPLAS 2019 -- XV International Conference on Computational Plasticity. Fundamentals and Applications, Barcelona (keynote lecture); 2019 \sep CMCS 2019 -- Computational Modeling of Complex Materials across the Scales (invited lecture);
2021 \sep PRIN2015 Metamaterials webinar~(invited online lecture);
2021 \sep METANANO 2021 -- VI International Conference on Metamaterials and Nanophotonics~(keynote online lecture);
2022 \sep ECCOMAS Congress 2022, session on Metamaterials Across the Scales:
Modeling, Experiment and Simulation~(keynote lecture)
}

\smallskip

\myConsulting{%
2004--2009 \sep Development of the finite element framework in C/C++ for GEO 5 FEM and GEO5 FEM-Tunnel systems, FINE Ltd.;
2005--2006 \sep Computational assessment of Charles Bridge in Prague using non-linear multiphysics finite element simulations, PUDIS a.s.;
2016--2017 \sep Reconstruction of discrete fracture networks from surface measurements, UJV Group, Řež}

\smallskip

\myFundingID{Principal and co-principal investigator of 7 projects awarded by the Czech Science Foundation~(2~evaluated as excellent) \sep 1 project awarded by Ministry of Industry and Trade of Czech Republic \sep 1~project awarded by Ministry of Education, Youth and, Sports \sep 1~Marie-Curie Intra-European Fellowship}

\smallskip

\myPublicationRecord{1 monograph, 2 book chapters, 75 journal articles}{1,774}{22}

\end{document}