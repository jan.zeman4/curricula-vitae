### Short bio: Jan Zeman

[comment]: <> (Current affiliation)
Dr. Jan Zeman is a Full Professor in the Theory of Materials and Structures at the Department of Mechanics, Faculty of Civil Engineering, Czech Technical University (CTU) in Prague.

[comment]: <> (Professional career in brief)
Currently a core member of the CTU’s Open Mechanics Group, Jan Zeman earned his Ing. (MSc. equivalent) degree in 2000 and his Ph.D. in 2003 from CTU in Civil Engineering with a specialization in Structural Mechanics. He received the 2000 Hlavka Talent Foundation prize (top CTU student) and the Ivo Babuška Award for the best Ph.D. thesis in Applied and Computational Mathematics and Mechanics in 2003.

[comment]: <> (Research interests & Notable funding)
Jan's research interests lie at the interface between applied mechanics and mathematics, with an emphasis on modeling and simulation of deterministic and stochastic microstructured materials, variational techniques for modeling inelastic materials, and mathematical analysis of engineering models and algorithms in general. His early work was supported in the context of a Marie-Curie Intra-European Fellowship (2005), and, in addition to leading 5 projects funded by the Czech Science Foundation (CSF) from 2004 to 2019, he is currently the principal investigator of a highly-selective five-year EXPRO project, a CSF initiative established in 2019, aiming at the computer-aided design of modular architectured materials. In addition to research, he is committed to mentoring talented future researchers and has (co-)supervised 8 doctoral students (1 ongoing).